import uuid
from flask import Flask, render_template, jsonify, request
from api_ai import api_ai as ai
from dao.pizza_dao import get_pizza

app = Flask(__name__)

APP_NAME = "PizzaBot"


@app.context_processor
def context():
    return dict(
        app_name=APP_NAME,
        js_version="0.4",
        css_version="0.3"
    )


@app.route('/')
def index():
    return render_template("index.html", session_id=str(uuid.uuid4()))


@app.route('/send-message')
def send_message():
    message = request.args.get("message")
    session_id = request.args.get("session_id")
    response = ai.send_message(message, session_id)
    return response


@app.route('/pizza-details/<pizza_id>')
def get_pizza_details(pizza_id):
    return jsonify(get_pizza(pizza_id))

if __name__ == '__main__':
    app.run(debug=True, port=5001)
