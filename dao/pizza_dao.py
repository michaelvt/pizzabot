PIZZAS = {
    "cheese": {
        "name": "Cheese",
        "description": "Lots of mozzarella cheese.",
        "image_name": "pizza_cheese.png"
    },
    "hawaiian": {
        "name": "Hawaiian",
        "description": "Smoked leg ham, pineapple and mozzarella.",
        "image_name": "pizza_hawaiian.png"
    },
    "meat": {
        "name": "Meat Lovers",
        "description": "Rasher bacon, pepperoni, cherry wood smoked leg ham, ground beef, Italian sausage on a BBQ sauce base.",
        "image_name": "pizza_meatlovers.png"
    },
    "pepperoni": {
        "name": "Pepperoni",
        "description": "Pepperoni & mozzarella.",
        "image_name": "pizza_pepperoni.png"
    },
    "vegetarian": {
        "name": "Vegetarian",
        "description": "Mushrooms, feta, sweet cherry peppers, baby spinach, capsicum, fresh tomato and red onion topped with oregano.",
        "image_name": "pizza_vegetarian.png"
    },
    "supreme": {
        "name": "Supreme",
        "description": "Pepperoni, rasher bacon, capsicum, ground beef, Italian sausage, mushroom, pineapple, topped with oregano & fresh sliced spring onion.",
        "image_name": "pizza_supreme.png"
    },
    "margherita": {
        "name": "Margherita",
        "description": "Cherry tomatoes, fresh tomato, basil drizzle & mozzarella.",
        "image_name": "pizza_margherita.png"
    },
}


def get_pizza(pizza_id):
    return PIZZAS.get(pizza_id, {})
