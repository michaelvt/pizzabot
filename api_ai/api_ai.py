import apiai

CLIENT_ACCESS_TOKEN = "f4476b4d251c41e39981c7efffdc21fd"

ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)


def send_message(message, session_id):
    request = ai.text_request()
    request.session_id = session_id
    request.query = message
    response = request.getresponse()
    return response.read()
