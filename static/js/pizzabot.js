/**
 * Created by Michael Van Treeck on 9/06/2017.
 */

var current_pizza = reset_order();
var mute_voice = true;

function reset_order() {
    return {
        "name": undefined,
        "image_name": undefined,
        "type": undefined,
        "description": undefined,
        "quantity": undefined,
        "crust": undefined,
        "size": undefined
    };
}

function send_message(message) {
    var session_id = $("#session_id").val();
    $.ajax({
        url: "/send-message",
        method: "get",
        data: {"session_id": session_id, "message": message},
        dataType: "json",
        success: function (data) {
            process_ai_response(data);
        },
        error: function (request, status, error) {
            alert("An unexpected error occurred!");
        }
    })
}

function process_ai_response(response) {

    var status = response["status"]["code"];
    if(status !== 200) {
        alert("An unexpected error occurred!");
        return;
    }

    var speech_response = response["result"]["fulfillment"]["speech"];
    add_chat_message(speech_response, true);
    if (!mute_voice)
        textToSpeech(speech_response);

    if(response["result"]["action"] === "order.pizza.restart") {
        current_pizza = reset_order();
        refresh_pizza_details();
    } else if(response["result"]["action"] === "order.pizza.confirm") {
        var order_desc = current_pizza["quantity"] + " x " + current_pizza["size"] + " " +current_pizza["type"] + " pizza with " +  current_pizza["crust"] +  " crust";
        add_chat_media("Order Details", order_desc, current_pizza["image_name"]);
        current_pizza = reset_order();
        refresh_pizza_details();
    } else {
        var order_status = response["result"]["parameters"];

        update_pizza_details(order_status);
        get_pizza_details(current_pizza["type"]);
    }
}

function get_pizza_details(pizza_id) {
    if(pizza_id)
        $.ajax({
            url: "/pizza-details/" + pizza_id,
            method: "get",
            dataType: "json",
            success: function (data) {
                update_pizza_details(data);
            },
            error: function (request, status, error) {
                alert("An unexpected error occurred!");
            }
        })
}

function update_pizza_details(data) {
    for(var k in current_pizza) {
        if (current_pizza.hasOwnProperty(k)) {
            if (data[k])
                current_pizza[k] = data[k];
        }
    }
    refresh_pizza_details();
}

function refresh_pizza_details() {
    var text_fields = ["name", "description", "quantity", "size", "crust"];

    if(current_pizza["name"]) {
        for (var k in text_fields) {
            if(text_fields.hasOwnProperty(k)) {
                var val = current_pizza[text_fields[k]];
                if (!val)
                    val = "";
                $("#pizza_" + text_fields[k]).text(val.capitalize());
            }
        }

        if (current_pizza["image_name"])
            $("#pizza_image_name").prop("src", "/static/images/" + current_pizza["image_name"]).show();
        else
            $("#pizza_image_name").hide();

        $("#no_pizza_details").hide();
        $("#pizza_details").fadeIn();
    } else {
        $("#no_pizza_details").show();
        $("#pizza_details").fadeOut();
    }
}

function add_chat_message(message, response) {
    response = response || false;

    var css_class = "chat-message-user pull-right";
    if(response) {
        css_class = "chat-message-ai pull-left";
    }
    var html = '<div class="row chat-message-row"><div class="panel panel-default chat-message ' + css_class + '"><div class="panel-body">' + message + '</div></div></div>';

    var $message_list = $("#message-list");
    $message_list.append(html);
    $message_list.animate({ scrollTop: $message_list.prop("scrollHeight")}, 1000);
}

function add_chat_media(heading, message, image) {
    image = image || "pizza_unknown.png";
    image = "/static/images/" + image;
    var html = '<div class="media"><div class="media-left"><img style="max-width:64px;" class="media-object" src="' +  image + '" alt="Pizza"></div><div class="media-body"><b>' + message + '</b></div></div>';
    add_chat_message(html, true);
}

$( document ).ready(function() {
    $("#send-message-form").submit(function (e) {
        e.preventDefault();  //prevent form from submitting
        var $new_message = $("#new_message");
        var message = $new_message.val().trim();
        if(message) {
            add_chat_message(message);
            send_message(message);
            $new_message.val("");
        }
    });

    $("#mute_voice").click(function () {
        mute_voice = true;
        $("#mute_voice, #unmute_voice").toggle();
    });

    $("#unmute_voice").click(function () {
        mute_voice = false;
        $("#mute_voice, #unmute_voice").toggle();
    });

    if (Modernizr.speechrecognition) {
        $("#startRecognition").parent().show();

        recognitionStartCallBack = function() {
            $("#message_input_group").hide();
        };

        recognitionStopCallBack = function () {
            $("#message_input_group").show();
        };

        recognitionCallBack = function(text) {
            $("#new_message").val(text);
            $("#send-message-form").submit();
        };

        loadSpeechRecognition();
    }

    if (Modernizr.speechsynthesis) {
        $(".speech-mute-options#mute_voice").show();
        mute_voice = false;
    }

});

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};
