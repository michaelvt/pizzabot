/**
 * Created by Michael Van Treeck on 13/06/17.
 */

/* Selectors for the starting/stopping of speech recognition */
var $recognitionStartSelector,
    $recognitionStopSelector;

/* Callback functions for the starting/stopping of speech recognition */
var recognitionStartCallBack,
    recognitionStopCallBack;

/* Callback for speech recognition (provides text as parameter) */
var recognitionCallBack;

function loadSpeechRecognition() {
    $recognitionStartSelector = $("#startRecognition");
    $recognitionStopSelector = $("#stopRecognition");

    $recognitionStartSelector.click(function(event) {
        switchRecognition();
    });

    $recognitionStopSelector.click(function () {
        stopRecognition();
    });
}

var recognition;
function startRecognition() {
    recognition = new webkitSpeechRecognition();
    recognition.onstart = function (event) {
        $recognitionStopSelector.show();
        if(isCallBack(recognitionStartCallBack))
            recognitionStartCallBack();
    };
    recognition.onresult = function (event) {
        var text = "";
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            text += event.results[i][0].transcript;
        }
        if(isCallBack(recognitionCallBack))
            recognitionCallBack(text);
        else
            console.log("No recognition call back set!");
        stopRecognition();
    };
    recognition.onend = function () {
        stopRecognition();
    };
    recognition.lang = "en-US";
    recognition.start();
}

function isCallBack(fn) {
    return typeof fn === "function";
}

function stopRecognition() {
    if (recognition) {
        recognition.stop();
        recognition = null;
    }
    $recognitionStopSelector.hide();
    if(isCallBack(recognitionStopCallBack))
        recognitionStopCallBack();
}

function switchRecognition() {
    if (recognition) {
        stopRecognition();
    } else {
        startRecognition();
    }
}

function textToSpeech(text) {
    if(text === "")
        return;

    var msg = new SpeechSynthesisUtterance();
    // var voices = window.speechSynthesis.getVoices();
    msg.voiceURI = "native";
    msg.text = text;
    // msg.lang = "en-AU";
    window.speechSynthesis.speak(msg);
}
