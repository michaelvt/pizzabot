# PizzaBot

## Synopsis

[PizzaBot](http://michaelv-pizzabot.herokuapp.com/) is a python web application (using Flask) that features a simple chat bot/conversational agent for ordering pizza. 

It interfaces with [Dialogflow](https://dialogflow.com/) (formally api.ai), an NLP conversational agent.

See the presentation on [chat bots](Chatbot demo.pdf) for more information.

## Motivation

This project was created to demonstrate how easy it is to interact with a conversational agent and add key features such as text to speech, and speech recognition.

## Examples

Type the following into the PizzaBot chat to get things started:

* _order pizza_
* _2 small cheese pizza with thin crust_
* _pepperoni pizza_
* _large hawaiian_

You can also try having some small talk, if that takes your fancy! Just start by saying _hi_.

## Installation

Python dependencies:
```sh
$ pip install --upgrade pip
$ pip install -r requirements.txt
```
To run:
```sh
$ python app.py
```

## Tests

There are currently no tests.

## Contributors

Created by Michael Van Treeck (email: michaelv.development@gmail.com)
